#import <UIKit/UIKit.h>
#import "DWUploadItem.h"
#import "DWDownloadItem.h"


@interface DWAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic)UIWindow *window;

@property (strong, nonatomic)DWDownloadItems *downloadFinishItems;
@property (strong, nonatomic)DWDownloadItems *downloadingItems;

@property (strong, nonatomic)DWUploadItems *uploadItems;

@end
