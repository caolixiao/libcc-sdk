#import <UIKit/UIKit.h>
#import "DWSDK.h"

@interface DWCustomPlayerViewController : UIViewController

@property (copy, nonatomic)NSString *videoId;
@property (copy, nonatomic)NSString *videoLocalPath;

@end
