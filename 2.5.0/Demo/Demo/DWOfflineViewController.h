#import <UIKit/UIKit.h>
#import "DWSDK.h"

@interface DWOfflineViewController : UIViewController

@property (strong, nonatomic)NSString *videoId;

@property (strong, nonatomic)NSString *playUrl;

@property (strong, nonatomic)NSString *definition;

@end
